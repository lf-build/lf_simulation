﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Simulation.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Mvc.Formatters;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.Simulation.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "BusinessSimulationServer"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Simulation.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<SimulationConfiguration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<ISimulationServerRepository, SimulationServerRepository>();
            services.AddTransient<ISimulationServerService, SimulationServerService>();

            services.AddTransient<IMongoConfiguration>(p =>
            {
                var configurationFactory = p.GetService<IConfigurationServiceFactory>();
                var currentTokenHandler = p.GetService<ITokenHandler>();
                var token = currentTokenHandler.Issue(Settings.TenantName, Settings.ServiceName);
                var staticTokenReader = new StaticTokenReader(token.Value);
                var defaultConfiguration = configurationFactory.Create<MongoConfiguration>("tenant", staticTokenReader).Get();
                var serviceconfiguration = configurationFactory.Create<MongoConfiguration>(Settings.ServiceName, staticTokenReader).Get();

                if (serviceconfiguration == null || string.IsNullOrWhiteSpace(serviceconfiguration.Database))
                    throw new Exception($"database property not found in  {Settings.ServiceName} configuration");

                MongoConfiguration mongoConfiguration = new MongoConfiguration
                {
                    Database = serviceconfiguration.Database,
                    ConnectionString = serviceconfiguration != null && !string.IsNullOrWhiteSpace(serviceconfiguration.ConnectionString) ? serviceconfiguration.ConnectionString : defaultConfiguration.ConnectionString
                };
                return mongoConfiguration;
            });

            services.AddTransient(p =>
            {
                var currentTokenHandler = p.GetService<ITokenHandler>();
                var token = currentTokenHandler.Issue(Settings.TenantName, Settings.ServiceName);
                var staticTokenReader = new StaticTokenReader(token.Value);
                var configuration = p.GetService<IConfigurationServiceFactory<SimulationConfiguration>>().Create(staticTokenReader).Get();
                return configuration;
            });

            services.AddMvc()
                .AddLendFoundryJsonOptions()
                .AddXmlSerializerFormatters()
                .AddXmlDataContractSerializerFormatters();
            services.AddMvc(option =>
            {
                option.InputFormatters.Add(new CustomInputFormatter());
                option.InputFormatters.Add(new XmlSerializerInputFormatter());
                option.OutputFormatters.Add(new XmlSerializerOutputFormatter());
            });
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Business SimulationServer Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
        }
    }
}