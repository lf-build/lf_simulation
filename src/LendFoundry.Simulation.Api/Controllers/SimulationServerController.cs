﻿#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Xml;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Simulation.Api.Controllers
{
    /// <summary>
    /// SimulationServerController class
    /// </summary>
    [Route("/")]
    public class SimulationServerController : Controller
    {
        /// <summary>
        /// constructor of SimulationServerController
        /// </summary>
        /// <param name="service"></param>
        public SimulationServerController(ISimulationServerService service)
        {
            if (service == null)
                throw new ArgumentNullException(nameof(service));

            Service = service;
        }

        private ISimulationServerService Service { get; }

        /// <summary>
        /// Adds the document.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">
        /// module
        /// or
        /// document
        /// </exception>
        [HttpPut("{module}/add")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult AddDocument([FromRoute]string module, [FromBody] AddDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            return new ObjectResult(Service.AddDocument(module, document));
        }

        /// <summary>
        /// Updates the document.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">
        /// module
        /// or
        /// document
        /// </exception>
        [HttpPut("{module}/update")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateDocument([FromRoute]string module, [FromBody] UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            return new ObjectResult(Service.UpdateDocument(module, document));
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">
        /// module
        /// or
        /// document
        /// </exception>
        [HttpPut("{module}/delete")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult DeleteDocument([FromRoute]string module, [FromBody] UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            return new ObjectResult(Service.DeleteDocument(module, document));
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="document">The document.</param>
        /// <param name="formdata">The formdata.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">module</exception>
        [HttpPost("{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public object GetDocument([FromRoute]string module, Dictionary<string, string> document, [FromBody]XmlNode formdata)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var header in HttpContext.Request.Headers)
            {
                headers.Add(header.Key, header.Value);
            }
   
            if ( module == "paynet" || module == "bankruptcy" || module == "criminalrecord" || module == "bankruptcyfcrareport")
            {
                return ((System.Xml.XmlNode)(Service.GetDocument(module, document, headers, formdata)));
            }
            else
            {
                return new ObjectResult(Service.GetDocument(module, document, headers, formdata));
            }
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="document">The document.</param>
        /// <param name="formdata">The formdata.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">module</exception>
        [HttpPost("json/{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetDocument([FromRoute]string module, Dictionary<string, string> document, [FromBody]object formdata)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var header in HttpContext.Request.Headers)
            {
                headers.Add(header.Key, header.Value);
            }
            var tmp=new { Main=formdata  };
            var response= Service.GetDocument(module, document, headers, JsonConvert.DeserializeXmlNode(JsonConvert.SerializeObject(tmp)),true);
            return Ok(response);
        }


        /// <summary>
        /// Gets the document get.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <param name="d">The d.</param>
        /// <param name="e">The e.</param>
        /// <param name="f">The f.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">module</exception>
        [HttpGet("{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetDocumentGet([FromRoute]string module,
            [FromRoute] string a,
            [FromRoute] string b,
            [FromRoute] string c,
            [FromRoute] string d,
            [FromRoute] string e,
            [FromRoute] string f)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            Dictionary<string, string> paramObject = new Dictionary<string, string>();
            paramObject.Add("a", a);
            paramObject.Add("b", b);
            paramObject.Add("c", c);
            paramObject.Add("d", d);
            paramObject.Add("e", e);
            paramObject.Add("f", f);

            if (Request.QueryString.HasValue)
            {
                string[] values = Request.QueryString.Value.Substring(1).Split(new char[] { '&' });

                foreach (var val in values)
                {
                    string[] vals = val.Split(new char[] { '=' });
                    paramObject.Add(vals[0], vals[1]);
                }
            }

            return new ObjectResult(Service.GetByGetDocument(module, paramObject));
        }

        [HttpPost("cibil")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetDocumentCibil([FromBody] Envelope document)
        {
            return new ObjectResult(Service.GetCibilDocument("cibil", document));
        }
    }
}