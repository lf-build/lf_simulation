﻿#if DOTNET2

using Microsoft.AspNetCore.Hosting;
using System;

namespace LendFoundry.Simulation.Api
{
    /// <summary>
    /// Entrypoint
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entrypoint
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }
        }
    }
}

#endif