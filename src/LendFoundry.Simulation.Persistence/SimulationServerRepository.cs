﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace LendFoundry.Simulation.Persistence
{
    public class SimulationServerRepository : MongoRepository<ISimulationServerDocuments, SimulationServerDocuments>, ISimulationServerRepository
    {
        static SimulationServerRepository()
        {
            BsonClassMap.RegisterClassMap<SimulationServerDocuments>(map =>
            {
                map.AutoMap();
                var type = typeof(SimulationServerDocuments);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public SimulationServerRepository(IMongoConfiguration configuration)
            : base(new FaleTenantService(), configuration, "SimulationServerDocuments")
        {
            CreateIndexIfNotExists("tenantId_moduleName", Builders<ISimulationServerDocuments>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ModuleName));
            CreateIndexIfNotExists("moduleName", Builders<ISimulationServerDocuments>.IndexKeys.Ascending(i => i.ModuleName));
        }

    }

    public class FaleTenantService : ITenantService
    {
        public TenantInfo Current
        {
            get
            {
                return new TenantInfo() { Id = Settings.TenantName };
            }
        }

        public List<TenantInfo> GetActiveTenants()
        {
            throw new NotImplementedException();
        }
    }
}
