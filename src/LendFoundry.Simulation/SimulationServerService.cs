﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LendFoundry.Simulation
{
    public class SimulationServerService : ISimulationServerService
    {
        public SimulationServerService(ISimulationServerRepository repository)
        {
            if (repository == null)
                throw new ArgumentNullException(nameof(repository));

            Repository = repository;
        }

        private ISimulationServerRepository Repository { get; }

        public string AddDocument(string module, AddDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));
            if (document.Document == null)
                throw new ArgumentNullException(nameof(document.Document));

            ISimulationServerDocuments dbDocument = new SimulationServerDocuments();
            try
            {
                ISimulationServerDocuments updatedbDocument = Repository.All(i => i.ModuleName == module && i.IsXml == document.IsXml && i.MinimumMatchRequiredForSearch == document.MinimumMatchRequiredForSearch
                                           && i.SearchParameters.Equals(document.SearchParameters)).Result.FirstOrDefault();
                if (updatedbDocument != null)
                {
                    UpdateDocument updatedoc = new UpdateDocument()
                    {
                        Document = document.Document,
                        DocumentId = updatedbDocument.Id,
                        IsXml = document.IsXml,
                        MinimumMatchRequiredForSearch = document.MinimumMatchRequiredForSearch,
                        SearchParameters = document.SearchParameters


                    };
                    UpdateDocument(module, updatedoc);
                    return updatedbDocument.Id;
                }
            }
            catch
            {

            }
            dbDocument.ModuleName = module.ToLower();
            dbDocument.SearchParameters = document.SearchParameters;
            dbDocument.IsXml = document.IsXml;
            dbDocument.IsXmlOutPut = document.IsXmlOutPut;
            dbDocument.Document = document.Document;
            dbDocument.MinimumMatchRequiredForSearch = document.MinimumMatchRequiredForSearch;

            Repository.Add(dbDocument);

            return dbDocument.Id;
        }

        public string UpdateDocument(string module, UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));
            if (document.Document == null)
                throw new ArgumentNullException(nameof(document.Document));

            ISimulationServerDocuments dbDocument = null;

            try
            {
                dbDocument = Repository.Get(document.DocumentId).Result;
            }
            catch
            {
            }

            if (dbDocument != null)
            {
                dbDocument.ModuleName = module.ToLower();
                dbDocument.SearchParameters = document.SearchParameters;
                dbDocument.IsXml = document.IsXml;
                dbDocument.IsXmlOutPut = document.IsXmlOutPut;
                dbDocument.Document = document.Document;
                dbDocument.MinimumMatchRequiredForSearch = document.MinimumMatchRequiredForSearch;

                Repository.Update(dbDocument);

                return dbDocument.Id;
            }

            return "Invalid Id";
        }

        public string DeleteDocument(string module, UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document)); ;

            ISimulationServerDocuments dbDocument = null;

            try
            {
                dbDocument = Repository.Get(document.DocumentId).Result;
            }
            catch
            {
            }

            if (dbDocument != null)
            {
                Repository.Remove(dbDocument);

                return dbDocument.Id;
            }
            return "Invalid Id";
        }

        public object GetCibilDocument(string module, Envelope request)
        {
            object returnValue = null;
            List<ISimulationServerDocuments> allDocuments = null;
            try
            {
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
            }
            catch (Exception)
            {
            }
            XDocument xDocTarget = null;
            if (allDocuments != null)
            {
                foreach (var document in allDocuments)
                {
                    if (document.IsXml)
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            try
                            {
                                xDocTarget = XDocument.Parse(document.Document);
                            }
                            catch (Exception)
                            {
                                // throw new Exception(document.SearchParameters+""+ex.StackTrace);
                            }
                            try
                            {
                                int matchCount = 0;
                                foreach (var kvp in document.SearchParameters)
                                {
                                    XElement source = null;
                                    try
                                    {
                                        if (source == null)
                                        {
                                            //   var doc = XDocument.Parse("<?xml version=\"1.0\" encoding=\"utf-8\"?><DCRequest xmlns=\"http://transunion.com/dc/extsvc\"><Authentication type=\"OnDemand\"><UserId>CreditExchange_Admin</UserId><Password>Password@123</Password></Authentication><RequestInfo><SolutionSetId>1135</SolutionSetId><SolutionSetVersion>26</SolutionSetVersion><ExecutionMode>NewWithContext</ExecutionMode>		</RequestInfo><UserData /><Fields><Field key=\"EnvironmentType\">U</Field><Field key=\"InpEnquiryPurpose\">01</Field><Field key=\"InpScoreType\">04</Field><Field key=\"ApplicantFirstName\">Mabel</Field><Field key=\"ApplicantMiddleName\"></Field><Field key=\"ApplicantLastName\">Rajagopalan</Field><Field key=\"ExternalApplicationId\">231243214</Field><Field key=\"Gender\">Female</Field><Field key=\"DateOfBirth\">02/06/1981</Field><Field key=\"EmailID\"></Field><Field key=\"PanNo\">ADZPR4147H</Field><Field key=\"DLNumber\"></Field><Field key=\"VoterId\">UBV2099604</Field><Field key=\"AdharNumber\">898230945645</Field><Field key=\"PassportNumber\"></Field><Field key=\"RationCardNo\"></Field><Field key=\"AdditionalID1\"></Field><Field key=\"AdditionalID2\"></Field><Field key=\"Address1Line1\">501 Fifth Floor</Field><Field key=\"Address1Line2\">Pam Ville DMonte Park</Field><Field key=\"Address1Line3\"></Field><Field key=\"Address1City\">Mumbai</Field><Field key=\"Address1StateCode\">27</Field><Field key=\"Address1Pincode\">400050</Field><Field key=\"Address1Category\">01</Field><Field key=\"Address1ResCode\">01</Field><Field key=\"Address2Line1\">HSBC Bank India</Field><Field key=\"Address2Line2\">Business Park</Field><Field key=\"Address2Line3\"></Field><Field key=\"Address2City\">Mumbai</Field><Field key=\"Address2StateCode\">27</Field><Field key=\"Address2Pincode\">400013</Field><Field key=\"Address2Category\"></Field><Field key=\"Address2ResCode\"></Field><Field key=\"TelePhoneNumber1\">9012783067</Field><Field key=\"TelePhoneNumber1Type\">01</Field><Field key=\"TelePhoneNumber2\">9345678109</Field><Field key=\"TelePhoneNumber2Type\">02</Field><Field key=\"TelePhoneNumber3\"></Field><Field key=\"TelePhoneNumber3Type\">03</Field><Field key=\"TelePhoneNumber4\"></Field><Field key=\"TelePhoneNumber4Type\"></Field><Field key=\"LoanAmount\">800000</Field><Field key=\"AccountNumber1\"></Field><Field key=\"AccountNumber2\"></Field><Field key=\"AccountNumber3\"></Field><Field key=\"AccountNumber4\"></Field><Field key=\"IsCIRReq\">Y</Field><Field key=\"IsEverifyReq\">Y</Field><Field key=\"ClientID\">21</Field><Field key=\"ekyc\"></Field></Fields></DCRequest>");//request.Body.ExecuteXMLString.Request
                                            var doc = XDocument.Parse(request.Body.ExecuteXMLString.Request);//
                                            XNamespace ns = "http://transunion.com/dc/extsvc";
                                            XElement xelement = doc.Elements(ns + "DCRequest").FirstOrDefault();
                                            var x = (from phoneno in xelement.Elements(ns + "Fields")

                                                     select phoneno);
                                            XElement fielddata = x.Elements(ns + "Field").Where(t => (string)t.Attribute("key") == "Applicants")?.FirstOrDefault();
                                            var cdatanode = fielddata.FirstNode.ToString().Replace("<![CDATA[", "").Replace("]]>", "");
                                            var applicants = XDocument.Parse(cdatanode);
                                            source = applicants.Elements("Applicants").Elements("Applicant").Elements("Telephones").Elements("Telephone").Where(i => i.Element("TelephoneType").Value == "01").Elements("TelephoneNumber")?.FirstOrDefault();
                                            //source = doc.XPathSelectElement("/*[local-name()='DCRequest']/*[local-name()='Fields']/*[local-name()='Field']");
                                            if (source != null)
                                            {
                                                var destination = string.Empty;

                                                if (kvp.Value.StartsWith("Value('"))
                                                {
                                                    destination = kvp.Value.Substring(7, kvp.Value.Length - 9);
                                                }
                                                else
                                                {
                                                    destination = xDocTarget.XPathSelectElement(kvp.Value).Value;
                                                }

                                                if (!string.IsNullOrWhiteSpace(source.Value) && !string.IsNullOrWhiteSpace(destination)
                                                    && source.Value.ToLower().Equals(destination.ToLower()))
                                                {
                                                    matchCount = matchCount + 1;
                                                }

                                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                                {
                                                    returnValue = document.Document;

                                                    return returnValue;
                                                }
                                                // break;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
            return returnValue;
        }

        public object GetDocument(string module, Dictionary<string, string> postedParams, Dictionary<string, string> headers, XmlNode formdata, bool postAsJSON = false)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            object returnValue = null;
            bool flag = false;
            List<ISimulationServerDocuments> allDocuments = null;
            XmlDocument xDocSource = new XmlDocument();
            try
            {
                module = module?.ToLower();
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
                if (module == "ExperianUrl")
                {
                    return allDocuments.Select(d=>d.Document).FirstOrDefault();
                }
            }
            catch (Exception)
            {
            }

            if (allDocuments != null)
            {
                foreach (var document in allDocuments)
                {
                    if (document.IsXml || postAsJSON)
                    {
                        if (postedParams!=null && postedParams.Count != 0)
                        {
                            if (module == "bpr")
                            {
                                xDocSource.LoadXml(postedParams["efx_request"]);
                            }
                            else
                            {
                                xDocSource.LoadXml(postedParams.FirstOrDefault().Value);
                            }
                        }
                        else if (formdata != null)
                        {
                            if (module == "teletrack")
                            {
                                xDocSource.LoadXml(formdata.ChildNodes[1].InnerXml);
                            }
                            else if(module=="clear")
                            {
                                xDocSource.LoadXml(formdata.OuterXml);
                            }
                            else if (module == "bcir")
                            {
                                xDocSource.LoadXml(formdata.OuterXml);
                            }
                            else if (module.ToLower() == "bpr")
                            {
                                xDocSource.LoadXml(formdata.OuterXml);
                            }
                            else
                            {
                                xDocSource.LoadXml(formdata.InnerXml);
                            }
                        }
                        else if (headers.Count != 0)
                        {
                            if (module == "bpr")
                            {
                                xDocSource.LoadXml(headers["efx_request"]);
                            }
                            else
                            {
                                xDocSource.LoadXml(headers.FirstOrDefault().Value);
                            }
                        }
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            try
                            {
                                foreach (var kvp in document.SearchParameters)
                                {
                                    XmlNodeList elementList = xDocSource.GetElementsByTagName(kvp.Key);
                                    foreach (XmlElement element in elementList)
                                    {
                                        if (kvp.Key == element.Name && kvp.Value == element.InnerText)
                                        {
                                            returnValue = document.Document;
                                            if(postAsJSON==true){
                                                returnValue = JsonConvert.DeserializeObject(document.Document);
                                                return returnValue;
                                            }
                                            if (module == "paynet" || module == "bankruptcy" || module == "criminalrecord" || module == "bankruptcyfcrareport" || document.IsXmlOutPut)
                                            {
                                                XmlNode xmlnode = XmlStringToXmlNode(document.Document);
                                                flag = true;
                                                return ((System.Xml.XmlNode)(xmlnode));
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception) { }
                        }

                        if (flag == true)
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            if (module.ToLower() == "gettransactionsbyaccounts")
                            {
                                var replaceWith=postedParams.ContainsKey("accountId") ? postedParams["accountId"] : "999999";
                                document.Document = document.Document.Replace("##AccountId##", replaceWith);
                            }
                            dynamic sourceObject = JsonConvert.DeserializeObject(Convert.ToString(document.Document));
                            int matchCount = 0;
                            foreach (var kvp in document.SearchParameters)
                            {
                                string destinationVal = postedParams.ContainsKey(kvp.Key) ? postedParams[kvp.Key] : null;
                                destinationVal = (destinationVal != null && postedParams[kvp.Key].Contains("+")) ? destinationVal : System.Net.WebUtility.UrlDecode(destinationVal);
                                string serchValue = kvp.Value;
                                if (!string.IsNullOrWhiteSpace(serchValue) && !string.IsNullOrWhiteSpace(destinationVal) && serchValue == destinationVal)
                                {
                                    matchCount = matchCount + 1;
                                }

                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                {
                                    returnValue = JsonConvert.DeserializeObject(document.Document);

                                    return returnValue;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return returnValue;
            }
            return returnValue;
        }

        public object GetByGetDocument(string module, Dictionary<string, string> paramObject)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            object returnValue = null;
            string serchValue = null;
            List<ISimulationServerDocuments> allDocuments = null;
            try
            {
                module = module?.ToLower();
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
            }
            catch (Exception)
            {
            }

            if (allDocuments != null)
            {
                foreach (var document in allDocuments)
                {
                    if (document.IsXml)
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {

                            int matchCount = 0;
                            foreach (var kvp in document.SearchParameters)
                            {
                                var sourceValue = paramObject.ContainsKey(kvp.Key) ? paramObject[kvp.Key] : null; 
                              
                                    if (kvp.Value == sourceValue)
                                    {
                                        returnValue = document.Document;
                                        matchCount = matchCount + 1;

                                    }
                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                {
                                    returnValue = document.Document;

                                    return returnValue;
                                }
                            }
                        }
                        else
                        {
                            returnValue = JsonConvert.DeserializeObject(document.Document);

                            return returnValue;
                        }
                    }
                    else
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            if (module.ToLower() == "gettransactionsbyaccounts")
                            {
                                var replaceWith = paramObject.ContainsKey("accountId") ? paramObject["accountId"] : "999999";
                                document.Document = document.Document.Replace("##AccountId##", replaceWith);
                            }
                            dynamic sourceObject = JsonConvert.DeserializeObject(Convert.ToString(document.Document));
                            int matchCount = 0;
                            foreach (var kvp in document.SearchParameters)
                            {
                                string destinationVal = paramObject.ContainsKey(kvp.Key) ? paramObject[kvp.Key] : null;
                                destinationVal = (destinationVal != null && paramObject[kvp.Key].Contains("+")) ? destinationVal : System.Net.WebUtility.UrlDecode(destinationVal);
                                serchValue = kvp.Value;
                                if (!string.IsNullOrWhiteSpace(serchValue) && !string.IsNullOrWhiteSpace(destinationVal) && serchValue == destinationVal)
                                {
                                    matchCount = matchCount + 1;
                                }

                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                {
                                    returnValue = JsonConvert.DeserializeObject(document.Document);

                                    return returnValue;
                                }
                            }
                        }
                    }
                }
            }

            return returnValue;
        }

        public XmlNode XmlStringToXmlNode(string xmlInputString)
        {
            if (String.IsNullOrEmpty(xmlInputString.Trim())) { throw new ArgumentNullException("xmlInputString"); }
            var xd = new XmlDocument();
            using (var sr = new StringReader(xmlInputString))
            {
                xd.Load(sr);
            }
            return xd;
        }
    }
}