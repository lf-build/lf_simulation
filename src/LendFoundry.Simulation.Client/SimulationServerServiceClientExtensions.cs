﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Simulation.Client
{
    public static class SimulationServerServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddSimulationServerService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ISimulationServerServiceClientFactory>(p => new SimulationServerServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ISimulationServerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSimulationServerService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<ISimulationServerServiceClientFactory>(p => new SimulationServerServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<ISimulationServerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSimulationServerService(this IServiceCollection services)
        {
            services.AddTransient<ISimulationServerServiceClientFactory>(p => new SimulationServerServiceClientFactory(p));
            services.AddTransient(p => p.GetService<ISimulationServerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

    }
}
