﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Xml;

namespace LendFoundry.Simulation.Client
{
    public class SimulationServerServiceClient : ISimulationServerService
    {
        public SimulationServerServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public string AddDocument(string module, AddDocument document)
        {
            throw new NotImplementedException();
        }

        public string DeleteDocument(string module, UpdateDocument document)
        {
            throw new NotImplementedException();
        }
      
        //public string AddDocument(string module, AddDocument document)
        //{
        //    var request = new RestRequest("/{module}/add", Method.PUT);
        //    request.AddUrlSegment("module", module);
        //    request.AddJsonBody(document);
        //    return Client.Execute<string>(request);
        //}

        //public string UpdateDocument(string module, UpdateDocument document)
        //{
        //    var request = new RestRequest("/{module}/update", Method.PUT);
        //    request.AddUrlSegment("module", module);
        //    request.AddJsonBody(document);
        //    return Client.Execute<string>(request);
        //}

        //public string DeleteDocument(string module, UpdateDocument document)
        //{
        //    var request = new RestRequest("/{module}/delete", Method.PUT);
        //    request.AddUrlSegment("module", module);
        //    request.AddJsonBody(document);
        //    return Client.Execute<string>(request);
        //}

        public object GetByGetDocument(string module, Dictionary<string, string> paramObject)
        {
            #region "Commented code other logic"

            //string objParam = string.Empty;
            //foreach (KeyValuePair<string, string> pair in paramObject)
            //{
            //    //Console.WriteLine("{0}, {1}", pair.Key, pair.Value);
            //    if (string.IsNullOrEmpty(objParam))
            //        objParam = pair.Value;
            //    else
            //        objParam += "/" + pair.Value;
            //}
            //var request = new RestRequest("/{module}/{param}", Method.GET);
            //request.AddUrlSegment("module", module);
            //request.AddUrlSegment("param", objParam);

            #endregion "Commented code other logic"

            var request = new RestRequest("/{module}/{a}/{b}/{c}/{d}/{e}/{f}", Method.GET);

            request.AddUrlSegment("module", module);

            if (paramObject.ContainsKey("a"))
            {
                request.AddUrlSegment("a", paramObject["a"]);
            }
            if (paramObject.ContainsKey("b"))
            {
                request.AddUrlSegment("b", paramObject["b"]);
            }

            if (paramObject.ContainsKey("c"))
            {
                request.AddUrlSegment("c", paramObject["c"]);
            }

            if (paramObject.ContainsKey("d"))
            {
                request.AddUrlSegment("d", paramObject["d"]);
            }

            if (paramObject.ContainsKey("e"))
            {
                request.AddUrlSegment("e", paramObject["e"]);
            }

            if (paramObject.ContainsKey("f"))
            {
                request.AddUrlSegment("f", paramObject["f"]);
            }

            return Client.ExecuteAsync<object>(request);
        }

        public object GetCibilDocument(string module, Envelope request)
        {
            throw new NotImplementedException();
        }

        public object GetDocument(string module, Dictionary<string, string> postedParams, Dictionary<string, string> headers, XmlNode formdata)
        {
            var request = new RestRequest("/{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}", Method.POST);
            foreach (var keyValue in headers)
                request.AddHeader(keyValue.Key, keyValue.Value);

            request.AddUrlSegment("module", module);

            request.AddJsonBody(postedParams);
            return Client.ExecuteAsync<object>(request);
        }

        public object GetDocument(string module, Dictionary<string, string> postedParams, Dictionary<string, string> headers, XmlNode formparams, bool postAsJSON = false)
        {
            var request = new RestRequest("/json/{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}", Method.POST);
            foreach (var keyValue in headers)
                request.AddHeader(keyValue.Key, keyValue.Value);

            request.AddUrlSegment("module", module);

            request.AddJsonBody(postedParams);
            return Client.ExecuteAsync<object>(request);
        }

        public string UpdateDocument(string module, UpdateDocument document)
        {
            throw new NotImplementedException();
        }
    }
}