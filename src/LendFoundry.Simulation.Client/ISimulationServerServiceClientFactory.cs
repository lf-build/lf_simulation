﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Simulation.Client
{
    public interface ISimulationServerServiceClientFactory
    {
        ISimulationServerService Create(ITokenReader reader);
    }
}
