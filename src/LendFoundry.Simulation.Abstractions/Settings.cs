﻿using System;

namespace LendFoundry.Simulation
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "simulation-server";
        public static string TenantName => Environment.GetEnvironmentVariable($"TENANT_NAME") ?? "my-tenant";
    }
}