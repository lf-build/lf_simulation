﻿using LendFoundry.Foundation.Persistence;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace LendFoundry.Simulation
{
    public class SimulationServerDocuments : Aggregate, ISimulationServerDocuments
    {
        public string ModuleName { get; set; }
        [BsonDictionaryOptions(Representation = MongoDB.Bson.Serialization.Options.DictionaryRepresentation.ArrayOfArrays)]
        public Dictionary<string, string> SearchParameters { get; set; }
        public int MinimumMatchRequiredForSearch { get; set; }
        public bool IsXml { get; set; }
        public  bool IsXmlOutPut { get; set; }
        public string Document { get; set; }
    }
}
