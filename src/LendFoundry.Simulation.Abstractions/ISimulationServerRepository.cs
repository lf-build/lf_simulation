﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Simulation
{
    public interface ISimulationServerRepository : IRepository<ISimulationServerDocuments>
    {

    }
}
