﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Simulation
{
    public class SimulationConfiguration : IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
