﻿using System.Collections.Generic;

namespace LendFoundry.Simulation
{
    public class AddDocument
    {
        public Dictionary<string, string> SearchParameters { get; set; }
        public int MinimumMatchRequiredForSearch { get; set; }
        public bool IsXml { get; set; }
        public bool IsXmlOutPut { get; set; }
        public string Document { get; set; }
    }
}
