﻿using System.Collections.Generic;
using System.Xml;

namespace LendFoundry.Simulation
{
    public interface ISimulationServerService
    {
        string AddDocument(string module, AddDocument document);

        string DeleteDocument(string module, UpdateDocument document);

        object GetDocument(string module, Dictionary<string, string> postedParams, Dictionary<string, string> headers, XmlNode formparams,bool postAsJSON=false);

        string UpdateDocument(string module, UpdateDocument document);

        object GetByGetDocument(string module, Dictionary<string, string> paramObject);

        object GetCibilDocument(string module, Envelope request);


    }
}